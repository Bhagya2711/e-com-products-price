package com.demo.price.service;

import com.demo.price.entity.Price;
import com.demo.price.exception.ProductPrice;

public interface PriceService {
	Price findprice(int pid) throws ProductPrice ;
}
